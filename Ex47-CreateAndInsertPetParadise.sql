/* 
USE <enter your database name>
GO

DROP TABLE TREATMENT
DROP TABLE PET
DROP TABLE OWNER
GO
*/

CREATE TABLE OWNER(
	OwnerId			Int				IDENTITY(1,1) NOT NULL,
	OwnerFirstName	NVarChar(30)	NOT NULL,
	OwnerLastName	NVarChar(30)	NOT NULL,
	OwnerPhone		NVarChar(20)	NULL,
	OwnerEmail		NVarChar(100)	NOT NULL,
	CONSTRAINT		Owner_PK		PRIMARY KEY (OwnerId)
)

CREATE TABLE PET (
	PetId 		Int 			IDENTITY(1,1) NOT NULL,
	PetName		NVarChar(30)	NOT NULL,
	PetType		NChar(3)		NOT NULL,
	PetBreed	NVarChar(30)	NOT NULL,
	PetDOB		DateTime2		NULL,
	PetWeight	Float			NOT NULL,
	OwnerId		Int				NOT NULL,
	CONSTRAINT	Pet_PK			PRIMARY KEY (PetId),
	CONSTRAINT	Pet_Owner_FK	FOREIGN KEY (OwnerId) REFERENCES OWNER(OwnerId)
)

CREATE TABLE TREATMENT (
	TreatId 		Int 			IDENTITY(1,1) NOT NULL,
	TreatService	NVarChar(100)	NOT NULL,
	TreatDate		DateTime2		NOT NULL,
	TreatCharge		Float			NOT NULL,
	PetId			Int				NULL,
	CONSTRAINT		Treatment_PK	PRIMARY KEY (TreatId),
	CONSTRAINT		Treatment_Pet_FK FOREIGN KEY (PetId) REFERENCES PET(PetId)
)
GO

INSERT INTO OWNER	(OwnerLastName,	OwnerFirstName,	OwnerPhone,		OwnerEmail)
		VALUES		('Downs',		'Marsha',		'555 537 8765',	'Marsha.Downs@somewhere.com'),
					('James',		'Richard',		'555 537 7654',	'Richard.James@somewhere.com'),
					('Frier',		'Liz',			'555 537 6543',	'Liz.Frier@somewhere.com'),
					('Trent',		'Miles',		NULL,			'Miles.Trent@somewhere.com'),
					('Evans',		'Hilary',		'210-634-2345',	'Hilary.Evans@somewhere.com')


INSERT INTO PET (PetName,	PetType,	PetBreed,		PetDOB,		PetWeight,	OwnerId)
		VALUES	('King',	'Dog',		'Std. Poodle',	'2011-02-21',	25.5,	1),
				('Teddy',	'Cat',		'Cashmere',		'2012-02-01',	10.5,	2),
				('Fido',	'Dog',		'Std. Poodle',	'2010-07-17',	28.5,	1),
				('AJ',		'Dog',		'Collie Mix',	'2011-05-05',	20.0,	3),
				('Cedro',	'Cat',		'Unknown',		'2009-06-06',	9.5,	2),
				('Woolley',	'Cat',		'Unknown',		NULL,			9.5,	2),
				('Buster',	'Dog',		'Border Collie','2008-12-11',	25.0,	4),
				('Jedah',	'Cat',		'Abyssinian',	'2006-07-01',	7.8,	5)

INSERT INTO TREATMENT	(TreatService,			TreatDate,		TreatCharge,	PetId)
			VALUES		('Ear Infection',		'2014-08-17',	65.00,			1),
						('Nail Clip',			'2014-09-05',	27.50,			2),
						('Parasites',			'2014-10-13',	42.00,			2),
						('One year shots',		'2014-05-05',	42.50,			4),
						('Ear infection',		'2014-09-24',	31.00,			4),
						('Nail Clip',			'2014-09-05',	27.50,			5),
						('Skin lnfection',		'2014-10-03',	35.00,			6),
						('Laceration Repair',	'2014-10-05',	127.00,			7),
						('Booster Shots',		'2014-11-04',	111.00,			8)
GO

